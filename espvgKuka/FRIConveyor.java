package org.espvgKuka;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import com.kuka.basictoolbox.gripper.HandlingGripperState;
import com.kuka.common.ThreadUtil;
import com.kuka.roboticsAPI.conditionModel.BooleanIOCondition;

import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.Workpiece;
import com.kuka.roboticsAPI.motionModel.IMotionContainer;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;
import org.espvgKuka.workstation.PneumaticGripper;
import org.espvgKuka.workstation.PiecePlate;
import com.kuka.generated.ioAccess.ConveyorsIOGroup;

import com.kuka.connectivity.fastRobotInterface.FRIConfiguration;
import com.kuka.connectivity.fastRobotInterface.FRISession;
import com.kuka.connectivity.motionModel.smartServoLIN.ISmartServoLINRuntime;
import com.kuka.connectivity.motionModel.smartServoLIN.SmartServoLIN;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.motionModel.BasicMotions;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKey;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyBar;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyListener;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyAlignment;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyEvent;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLED;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLEDSize;

public class FRIConveyor extends RoboticsAPIApplication {

	private enum ApplicationState {initialize, waiting, picking, storing, refill, shutdown}

	private LBR _lbr;
	private FRISession _session;
	private Frame _dynamicFrame;
	private boolean _endTrack;
	private Frame destination; 
	private double _startingOffset;
	private double hight = 130;
	
	@Inject private PneumaticGripper _gripper;
	@Inject private ConveyorsIOGroup _conveyorGroup;
	@Inject private PiecePlate _plate;
	private Frame _lastdestination; 

	private ApplicationState _state = ApplicationState.initialize;
	
	@Override
	public void initialize() {
		_lbr = getContext().getDeviceFromType(LBR.class);
		_lbr.setHomePosition(new JointPosition(-Math.PI/4, 0, 0, -Math.PI/2, 0, Math.PI/2, 0));
		
		_gripper.attachTo(_lbr.getFlange());
		_dynamicFrame = new Frame(getFrame("/Conveyor_1"));
		_endTrack=false;
		
	}
	
	@Override
	public void run() throws Exception {
       
		IUserKeyBar gripperBar = getApplicationUI().createUserKeyBar("Gripper");
		
		IUserKeyListener listener = new IUserKeyListener() {
			
			@Override
			public void onKeyEvent(IUserKey key, UserKeyEvent event) {
				if (event==UserKeyEvent.FirstKeyDown){
					_gripper.gripAsync(); 
					key.setLED(UserKeyAlignment.Middle, UserKeyLED.Green, UserKeyLEDSize.Normal); 
										
				}
				if (event==UserKeyEvent.SecondKeyDown){
					_gripper.releaseAsync();
					key.setLED(UserKeyAlignment.Middle, UserKeyLED.Grey, UserKeyLEDSize.Small); 
										
				}
				
			}
		};
		
		IUserKey gripperOnOff = gripperBar.addDoubleUserKey(0, listener, true);
	
		if(_gripper.getGripperState()==HandlingGripperState.GRIPPED) {
			gripperOnOff.setLED(UserKeyAlignment.Middle, UserKeyLED.Green, UserKeyLEDSize.Normal);
		} else {
			gripperOnOff.setLED(UserKeyAlignment.Middle, UserKeyLED.Grey, UserKeyLEDSize.Normal);
		}

		gripperBar.publish();
		
		if (0 == getApplicationUI().displayModalDialog(ApplicationDialogType.WARNING, 
				"This application is intended to use with an external control communicating by FRI.\n" +
				"The behaviour of the robot can possibly be erratical and unpredictable, Do you wish to Continue?", "Exit", "Continue"))
			return;
		getLogger().info(">> Moving to Home.");
		_lbr.move(BasicMotions.ptpHome().setJointVelocityRel(0.2));
		
		FRIConfiguration friConf = FRIConfiguration.createRemoteConfiguration(_lbr, (String) getApplicationData().getProcessData("IP_CLIENT").getValue());
		friConf.setSendPeriodMilliSec(5);
		friConf.registerIO(_conveyorGroup.getInput("Conveyor01_Pos"));
		friConf.registerTransformationProvider("dynFrame", _dynamicFrame);
		friConf.setReceiveMultiplier(3);
	

		getLogger().info(">> Creating FRI connection to " + friConf.getHostName() + ":" + friConf.getPortOnRemote());
        getLogger().info(">> SendPeriod: " + friConf.getSendPeriodMilliSec() + "ms | ReceiveMultiplier: " + friConf.getReceiveMultiplier());
        _session = new FRISession(friConf);
       
        try {
        	getLogger().info("Waiting for GOOD or EXCELLENT connection..."); 
        	_session.await(100, TimeUnit.SECONDS);
        	
        } catch (TimeoutException e) {
        	getLogger().error("!! Connection didn't reach acceptable quality:\n" + e.getMessage());
        	return;
        }
          
        getLogger().info(">> FRI connection established.");
       

		while (_state != ApplicationState.shutdown && _session.getFRIChannelInformation().getQuality().ordinal() > 0) {
			boolean once = true;
			switch (_state) {
				case initialize:
					_lbr.moveAsync(BasicMotions.ptpHome().setJointVelocityRel(0.2));
					_gripper.releaseAsync();
					setState(ApplicationState.waiting);
					break;

				case waiting:
					
					_lbr.moveAsync(BasicMotions.ptp(getApplicationData().getFrame("/waitingPoint")).setJointVelocityRel(0.2));
					IMotionContainer container = _lbr.move(BasicMotions.positionHold(
					        new JointImpedanceControlMode(1500, 1500, 1500, 1500, 1500, 1500, 1500),1, TimeUnit.MINUTES)
                            .breakWhen(new BooleanIOCondition(_conveyorGroup.getInput("CameraTrigger"), true)));

					if (container.getFiredBreakConditionInfo() != null) {
						_gripper.releaseAsync();
						setState(ApplicationState.picking); 
					}

					break;

				case picking:
					
					if(_dynamicFrame.getX()<=_startingOffset){  //first value 250mm (Distance between nut and vision area boundary) checks if the piece is outside of the vision area
						break; 
					} 
						
						SmartServoLIN smrtLin = new SmartServoLIN(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()));
						smrtLin.setMinimumTrajectoryExecutionTime(0.05); //old 0.1
						smrtLin.setSpeedTimeoutAfterGoalReach(0.1);
						_gripper.getDefaultMotionFrame().moveAsync(smrtLin);
						final ISmartServoLINRuntime rt = smrtLin.getRuntime();
						rt.activateVelocityPlanning(true);
					/*	rt.setGoalReachedEventHandler(new IServoOnGoalReachedEvent() {
							@Override
							public void onGoalReachedEvent(String state, double[] currentAxisPos,
									int[] osTimestamp, int targetId) {
								//if (_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()).isCloseTo(destination, 10, 2*Math.PI)) {
								if (destination.getZ() > 115) {
									hight = 115;
								} else {
									_gripper.gripAsync();
									_endTrack = true;
									setState(ApplicationState.storing); 
								}
							}
						});*/
						
						_startingOffset = getApplicationData().getProcessData("baseOffset").getValue();
						hight = 130;
						do {	
							
							double directAlpha; 
							directAlpha = (_dynamicFrame.transformationFromWorld().getAlphaRad()); 
							destination = new Frame(_dynamicFrame.transformationFromWorld()).setAlphaRad(_dynamicFrame.getAlphaRad()+Math.PI/2)
											/*Math.abs(_dynamicFrame.transformationFromWorld().getGammaRad()))*/.setZ(hight).setBetaRad(0).setGammaRad(Math.toRadians(-180));
							//	if (destination.getAlphaRad()>100)
								if (destination.getAlphaRad()<0)
									destination.setAlphaRad(destination.getAlphaRad()+Math.PI); 
								else 
									destination.setAlphaRad(destination.getAlphaRad()-Math.PI);
									
								if (_dynamicFrame.getX()>_startingOffset) {
									if (once){
										getLogger().info("A(deg) = " + Math.toDegrees(_dynamicFrame.getAlphaRad()) + ";" + _dynamicFrame.toString());
//											getLogger().info(String.format("DESTINATION A = %.2f; Dynamic A= %.2f, Current A= %.2f", 
//													Math.toDegrees(destination.getAlphaRad()), Math.toDegrees(_dynamicFrame.getGammaRad()), 
//													Math.toDegrees(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()).transformationFromWorld().getAlphaRad())));
											once=false; 
								//			}
//										if (once){
//										getLogger().info(String.format("Dynamic A = %.2f; B= %.2f, C = %.2f", 
//												Math.toDegrees(_dynamicFrame.getAlphaRad()), Math.toDegrees(_dynamicFrame.getBetaRad()), 
//												Math.toDegrees(_dynamicFrame.getGammaRad())));
//										once=false; 
										}
										rt.updateWithRealtimeSystem();
										rt.setDestination(destination);
										if(destination.isCloseTo(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()), 4, Math.PI)) {
											getLogger().error("time : " + rt.getRemainingTime() ); 
											if (destination.getZ() > 115) {
												hight = 115;
											} else {
												_gripper.gripAsync();
												_endTrack = true;
												_lastdestination = destination; 
												getLogger().info(String.format("DESTINATION = %.2f; DYNAMIC = %.2f, CURRENT = %.2f, DIFF = %.2f", 
														Math.toDegrees(destination.getAlphaRad()), Math.toDegrees(_dynamicFrame.transformationFromWorld().getAlphaRad()), 
														Math.toDegrees(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()).getAlphaRad()),
														Math.toDegrees(directAlpha)- Math.toDegrees(_dynamicFrame.getAlphaRad())));
												setState(ApplicationState.storing); 
											}
										}
										
								}
							}while (!_endTrack || destination.getY()<=-255);
							
							rt.stopMotion();
							_endTrack=false; 
					break;

				case storing:
					_gripper.moveAsync(BasicMotions.ptp(getApplicationData().getFrame("/waitingPoint")).setJointVelocityRel(0.2).setBlendingCart(100));
                    _gripper.moveAsync(BasicMotions.ptp(_plate.getPlaceEntryPoint()).setJointVelocityRel(0.2).setBlendingCart(100));
                    _gripper.moveAsync(BasicMotions.ptp(_plate.getPlacePrePos()).setJointVelocityRel(0.2).setBlendingCart(50));
                    
                    _gripper.move(BasicMotions.lin(_plate.getPlaceGripPos()/*.copy().setAlphaRad(_lastdestination.getAlphaRad())*/).setJointVelocityRel(0.2));
                    _gripper.releaseAsync();
                    ThreadUtil.milliSleep(300);
                    _gripper.moveAsync(BasicMotions.ptp(_plate.getPlacePrePos()).setJointVelocityRel(0.2).setBlendingCart(20));
                    _gripper.moveAsync(BasicMotions.ptp(_plate.getPlaceEntryPoint()).setJointVelocityRel(0.2).setBlendingCart(20));
                    _plate.addWorkpiece(new Workpiece("Piece"), null);
                    if (_plate.isFull()){
                    	setState(ApplicationState.refill);
                    } else {                    	
                    	setState(ApplicationState.waiting);
                    }
					break;

                case refill:
                    while (!_plate.isEmpty()) {
                        _gripper.moveAsync(BasicMotions.ptp(_plate.getPickEntryPoint()).setJointVelocityRel(0.2).setBlendingCart(20));
                        _gripper.moveAsync(BasicMotions.ptp(_plate.getPickPrePos()).setJointVelocityRel(0.2).setBlendingCart(20));
                        _gripper.move(BasicMotions.lin(_plate.getPickPlacePos()).setJointVelocityRel(0.2));
                        _gripper.gripAsync();
                        ThreadUtil.milliSleep(300);
                        _gripper.moveAsync(BasicMotions.ptp(_plate.getPickPrePos()).setJointVelocityRel(0.2).setBlendingCart(50));
                        _gripper.moveAsync(BasicMotions.ptp(_plate.getPickEntryPoint()).setJointVelocityRel(0.2).setBlendingCart(100));
                        _plate.removeWorkpiece();

                        _gripper.move(BasicMotions.ptp(getFrame("/dropPieces")).setJointVelocityRel(0.2));
                        _gripper.releaseAsync();
                        ThreadUtil.milliSleep(300);
                    }
                    setState(ApplicationState.waiting);
                    break;

				default:
				case shutdown:
					return;
			}
		}
	}
	
	@Override
	public void dispose() {
		_endTrack=false;
		_gripper.releaseAsync(); 
		if (_session != null)
			_session.close();
		getLogger().info(">> disposed.");
		super.dispose();
	}
	
	private void setState(ApplicationState newState) {
		if (_state != newState) {
			getLogger().info(">> State Update: "+ _state.name() +" -> "+ newState.name());
			_state = newState;
		}
	}
}

package org.espvgKuka.fri;


public enum ApplicationStates {
	AWAIT,
	PAUSE,
	INITIALIZING,
	SHUTDOWN,
	RUN,
	STOPPED;
	
	public static ApplicationStates getValue(int index) {
		if (index > values().length)
			throw new StackOverflowError();
		return values()[index];
	}
}

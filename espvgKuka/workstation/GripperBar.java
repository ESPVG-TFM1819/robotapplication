/**
 * 
 */
package org.espvgKuka.workstation;

import com.kuka.basictoolbox.gripper.HandlingGripperState;
import com.kuka.roboticsAPI.uiModel.ITaskUI;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKey;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyBar;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyListener;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyAlignment;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyEvent;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLED;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLEDSize;

/**
 * @author schick
 * 
 */

public class GripperBar {
	
	private int toolNumber;
	private PneumaticGripper gripper;
	private ITaskUI task;
	private IUserKeyBar gripperBar;

	public GripperBar(ITaskUI iTaskUI, PneumaticGripper gripper) {
		this.task = iTaskUI;
		toolNumber = 1;
		this.gripper = gripper;

		showGripperBar();
	}

	private void showGripperBar() {
		
		gripperBar = task.createUserKeyBar("Gripper");

		IUserKeyListener openGripperListener = new IUserKeyListener() {
			@Override
			public void onKeyEvent(IUserKey key, UserKeyEvent event) {
				
				if (event == UserKeyEvent.KeyDown && gripper.getGripperState()==HandlingGripperState.RELEASED &&!(gripper.getGripperState()==HandlingGripperState.GRIPPED)) {
					gripper.gripAsync();
					key.setLED(UserKeyAlignment.BottomMiddle, UserKeyLED.Red,UserKeyLEDSize.Small);
					key.setText(UserKeyAlignment.TopMiddle, "OPEN");
				} else if (event == UserKeyEvent.KeyDown){
					gripper.releaseAsync();
					key.setLED(UserKeyAlignment.BottomMiddle, UserKeyLED.Green,UserKeyLEDSize.Small);
					key.setText(UserKeyAlignment.TopMiddle, "CLOSE");
				}
			}
		};
		
		IUserKeyListener toolListener = new IUserKeyListener() {
			
			@Override
			public void onKeyEvent(IUserKey key, UserKeyEvent event) {
				// TODO Auto-generated method stub
				if (event == UserKeyEvent.FirstKeyDown && toolNumber < 10) 
					toolNumber++;
				 else if(event == UserKeyEvent.SecondKeyDown && toolNumber > 1) 
					toolNumber--;
				
				key.setText(UserKeyAlignment.Middle, Integer.toString(toolNumber));
			}
		};

		
		IUserKey openKey = gripperBar.addUserKey(0, openGripperListener, true);
		openKey.setText(UserKeyAlignment.TopMiddle, "OPEN");
		openKey.setLED(UserKeyAlignment.BottomMiddle, UserKeyLED.Yellow,UserKeyLEDSize.Small);
		
		IUserKey toolKey = gripperBar.addDoubleUserKey(2, toolListener, true);
		toolKey.setText(UserKeyAlignment.TopMiddle, "+");
		toolKey.setText(UserKeyAlignment.Middle, Integer.toString(toolNumber));
		toolKey.setText(UserKeyAlignment.BottomMiddle, "-");

		gripperBar.publish();

	}

	/**
	 * @return the gripperBar
	 */
	public IUserKeyBar getGripperBar() {
		return gripperBar;
	}


}

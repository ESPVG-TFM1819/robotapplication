package org.espvgKuka.workstation;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.kuka.generated.ioAccess.GripperIOGroup;
import com.kuka.basictoolbox.gripper.HandlingGripperState;
import com.kuka.basictoolbox.gripper.IHandlingGripper;
import com.kuka.common.StatisticTimer;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.geometricModel.Workpiece;


public class PneumaticGripper extends Tool implements IHandlingGripper {

	@Inject private GripperIOGroup _ioGroup;
	private Workpiece _workpiece;
	
	public PneumaticGripper(String name) {
		super(name);
	}

	@Override
	public void releaseAsync() {
		_ioGroup.setCloseO(true);
		_ioGroup.setOpenO(false);
	}

	@Override
	public void gripAsync() {
		_ioGroup.setCloseO(false);
		_ioGroup.setOpenO(true);
	}

	@Override
	public HandlingGripperState getGripperState() {
		// TODO Get a cable M8 4pin to connect the gripper to the Mediaflange, as it is we will trust the Output values.
		if (_ioGroup.getCloseO() && !_ioGroup.getOpenO()) {
			return HandlingGripperState.RELEASED;
		} else if(!_ioGroup.getCloseO() && _ioGroup.getOpenO()) {
			return HandlingGripperState.RELEASED;//FIXME change value to gripp with piece.
		}
		return HandlingGripperState.RELEASED;
	}

	@Override
	public boolean waitForGripperState(int timeout, TimeUnit timeUnit, HandlingGripperState... target) {
		StatisticTimer timer = new StatisticTimer();
		StatisticTimer.OneTimeStep step;
		while (timer.getSumTimeMillis() > timeUnit.toMillis(timeout)) {
			step = timer.newTimeStep();
			for (HandlingGripperState state : target) {
				if (state == getGripperState()) {
					step.end();
					return true;
				}
			}
			step.end();
		}
		return false;
	}

	@Override
	public boolean waitForGripperState(HandlingGripperState... target) {
		return waitForGripperState(100, TimeUnit.MILLISECONDS, target);
	}

	@Override
	public void setWorkpiece(Workpiece workpiece) {
		_workpiece = workpiece;
	}

	@Override
	public Workpiece getWorkpiece() {
		return _workpiece;
	}

	@Override
	public Tool getTool() {
		return this;
	}
}

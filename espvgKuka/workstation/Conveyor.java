package org.espvgKuka.workstation;

import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

import com.kuka.generated.ioAccess.ConveyorsIOGroup;
import com.kuka.roboticsAPI.applicationModel.IApplicationData;
import com.kuka.roboticsAPI.geometricModel.SpatialObject;
import com.kuka.roboticsAPI.persistenceModel.processDataModel.IProcessData;

public class Conveyor extends SpatialObject {
	
	private class _signalObserver implements Observer {

		@Override
		public void update(Observable o, Object arg) {
			
		}
	}
	
	@Inject private ConveyorsIOGroup _ioGroup;
	@Inject private IApplicationData _appData;
	
	private IProcessData _conveyorData01, _conveyorData02;

	public Conveyor(String name) {
		super(name);
		_conveyorData01 = _appData.getProcessData("C01_speed");
		_conveyorData02 = _appData.getProcessData("C02_speed");
	}
	
	public void setfwd() {
		_ioGroup.setConveyors_controlWord(0x00001001);
	}
	
	public void setrev() {
		_ioGroup.setConveyors_controlWord(0x00010010);
	}

	public void setStop() {
		_ioGroup.setConveyors_controlWord(0x00100100);
	}
}

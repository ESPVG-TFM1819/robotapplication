package org.espvgKuka.workstation;

import java.util.ArrayList;

import javax.inject.Inject;

import com.kuka.basictoolbox.container.IContainer;
import com.kuka.roboticsAPI.geometricModel.*;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;

public class PiecePlate extends PhysicalObject implements IContainer {

	@Inject
	public PiecePlate(String name) {
		super(name);
        _workPieces = new ArrayList<Workpiece>(NUM_SLOTS);
    }
    
	private ArrayList<Workpiece> _workPieces;

    public static final double SPACE_SLOTS = 79;
    public static final int NUM_SLOTS = 2;

    
    @Override
    public String getId() {
        return getName();
    }

    @Override
    public PhysicalObject toPhysicalObject() {
        return this;
    }

    @Override
    public Workpiece getWorkpiece() {
        if (isEmpty()) return null;
        return _workPieces.get(_workPieces.size()-1);
    }

    @Override
    public void removeWorkpiece() {
        if (isEmpty()) throw new IllegalStateException("Container Empty");
        _workPieces.remove(size()-1);
    }

    @Override
    public AbstractFrame getPickEntryPoint() {
        return World.Current.getFrame("/Container/EntryPoint");
    }

    @Override
    public AbstractFrame getPickPrePos() {
        return ((Frame) getPickPlacePos()).transform(Transformation.ofTranslation(0, 0, -20));
    }

    @Override
    public AbstractFrame getPickPlacePos() {
        return World.Current.getFrame("/Container/Slot").copy().transform(Transformation.ofTranslation(0, (size()-1)*SPACE_SLOTS, 0));
    }

    @Override
    public void addWorkpiece(Workpiece workpiece, ObjectFrame objectFrame) {
        if (isFull()) throw new IllegalStateException("Container full");
        _workPieces.add(workpiece); 
    }

    @Override
    public AbstractFrame getPlaceEntryPoint() {
        return getPickEntryPoint();
    }

    @Override
    public AbstractFrame getPlacePrePos() {
        return ((Frame) getPlaceGripPos()).transform(Transformation.ofTranslation(0, 0, -20));
    }

    @Override
    public AbstractFrame getPlaceGripPos() {
        return World.Current.getFrame("/Container/Slot").copy().transform(Transformation.ofTranslation(0, size()*SPACE_SLOTS, 0));
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean isFull() {
        return size() == capacity();
    }

    @Override
    public int capacity() {
        return NUM_SLOTS;
    }

    @Override
    public int size() {
        return _workPieces.size();
    }
}
package org.inspectionApp;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import com.kuka.common.ThreadUtil;
import com.kuka.connectivity.fastRobotInterface.FRIConfiguration;
import com.kuka.connectivity.fastRobotInterface.FRISession;
import com.kuka.connectivity.motionModel.smartServoLIN.ISmartServoLINRuntime;
import com.kuka.connectivity.motionModel.smartServoLIN.SmartServoLIN;
import com.kuka.generated.ioAccess.ConveyorsIOGroup;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.conditionModel.BooleanIOCondition;
import com.kuka.roboticsAPI.conditionModel.ConditionObserver;
import com.kuka.roboticsAPI.conditionModel.ForceCondition;
import com.kuka.roboticsAPI.conditionModel.IRisingEdgeListener;
import com.kuka.roboticsAPI.conditionModel.JointTorqueCondition;
import com.kuka.roboticsAPI.conditionModel.NotificationType;
import com.kuka.roboticsAPI.conditionModel.TorqueComponentCondition;
import com.kuka.roboticsAPI.deviceModel.JointEnum;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.motionModel.BasicMotions;
import com.kuka.roboticsAPI.motionModel.IMotionContainer;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianImpedanceControlMode;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianSineImpedanceControlMode;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;

import org.espvgKuka.workstation.PneumaticGripper;

public class Inspection extends RoboticsAPIApplication {

	private enum ApplicationState {
		initialize, waiting, picking, Display, refill, drop, shutdown
	}

	private LBR _lbr;
	private FRISession _session;
	private Frame _dynamicFrame;
	private boolean _endTrack;
	private Frame destination;
	private double _startingOffset;
	private double hight = 130;

	private IMotionContainer container;
	private CartesianImpedanceControlMode cartMode;
	private boolean refusePiece;
	ConditionObserver PieceEvaluator;

	@Inject private PneumaticGripper _gripper;
	@Inject private ConveyorsIOGroup _conveyorGroup;

	private ApplicationState _state = ApplicationState.initialize;

	@Override
	public void initialize() {
		_lbr = getContext().getDeviceFromType(LBR.class);
		_lbr.setHomePosition(new JointPosition(-Math.PI / 4, 0, 0, -Math.PI / 2, 0, Math.PI / 2, 0));

		_gripper.attachTo(_lbr.getFlange());
		_dynamicFrame = new Frame(getFrame("/Conveyor_1"));
		_endTrack = false;

		cartMode = CartesianSineImpedanceControlMode.createDesiredForce(CartDOF.ROT, 0, 300);
		cartMode.parametrize(CartDOF.TRANSL).setAdditionalControlForce(500);

		PieceEvaluator = getObserverManager().createConditionObserver(
			new JointTorqueCondition(JointEnum.J1, -5, 5), NotificationType.EdgesOnly, new IRisingEdgeListener(){
				@Override
				public void onRisingEdge(ConditionObserver observer, Date date, int eventsMissed) {
					if (_lbr.getExternalTorque().getSingleTorqueValue(JointEnum.J1)>=((JointTorqueCondition)observer.getCondition()).getMaxTorque()) {
						getLogger().info("\t>> store piece");						
						refusePiece = false;
					} else {
						getLogger().info("\t>> Yeet piece");
						refusePiece = true;
					}
				}
			});
	}

	@Override
	public void run() throws Exception {
		if (0 == getApplicationUI().displayModalDialog(ApplicationDialogType.WARNING,
				"This application is intended to use with an external control communicating by FRI.\n"
						+ "The behaviour of the robot can possibly be erratical and unpredictable, Do you wish to Continue?",
				"Exit", "Continue"))
			return;
		getLogger().info(">> Moving to Home.");
		_lbr.move(BasicMotions.ptpHome().setJointVelocityRel(0.2));

		FRIConfiguration friConf = FRIConfiguration.createRemoteConfiguration(_lbr,
				(String) getApplicationData().getProcessData("IP_CLIENT").getValue());
		friConf.setSendPeriodMilliSec(5);
		friConf.registerIO(_conveyorGroup.getInput("Conveyor01_Pos"));
		friConf.registerTransformationProvider("dynFrame", _dynamicFrame);
		friConf.setReceiveMultiplier(3);

		getLogger().info(">> Creating FRI connection to " + friConf.getHostName() + ":" + friConf.getPortOnRemote());
		getLogger().info(">> SendPeriod: " + friConf.getSendPeriodMilliSec() + "ms | ReceiveMultiplier: "
				+ friConf.getReceiveMultiplier());
		_session = new FRISession(friConf);

		try {
			getLogger().info("Waiting for GOOD or EXCELLENT connection...");
			_session.await(100, TimeUnit.SECONDS);

		} catch (TimeoutException e) {
			getLogger().error("!! Connection didn't reach acceptable quality:\n" + e.getMessage());
			return;
		}

		getLogger().info(">> FRI connection established.");

		while (_state != ApplicationState.shutdown && _session.getFRIChannelInformation().getQuality().ordinal() > 0) {
			boolean once = true;
			switch (_state) {
			case initialize:
				_lbr.moveAsync(BasicMotions.ptpHome().setJointVelocityRel(0.2));
				_gripper.releaseAsync();
				setState(ApplicationState.waiting);
				break;

			case waiting:
				_lbr.moveAsync(BasicMotions.ptp(getApplicationData().getFrame("/waitingPoint")).setJointVelocityRel(0.2));
				container = _lbr.move(BasicMotions
						.positionHold(new JointImpedanceControlMode(1500, 1500, 1500, 1500, 1500, 1500, 1500), 1,
								TimeUnit.MINUTES)
						.breakWhen(new BooleanIOCondition(_conveyorGroup.getInput("CameraTrigger"), true)));

				if (container.getFiredBreakConditionInfo() != null) {
					_gripper.releaseAsync();
					setState(ApplicationState.picking);
				}
				break;

			case picking:
				/*
					first value 250mm (Distance between nut and vision
					area boundary) checks if the piece is outside of the
					vision area
				*/
				if (_dynamicFrame.getX() <= _startingOffset) { 
					break;
				}

				SmartServoLIN smrtLin = new SmartServoLIN(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()));
				smrtLin.setMinimumTrajectoryExecutionTime(0.05); // old 0.1
				smrtLin.setSpeedTimeoutAfterGoalReach(0.1);
				_gripper.getDefaultMotionFrame().moveAsync(smrtLin);
				final ISmartServoLINRuntime rt = smrtLin.getRuntime();
				rt.activateVelocityPlanning(true);

				_startingOffset = getApplicationData().getProcessData("baseOffset").getValue();
				hight = 130;
				do {
					double directAlpha;
					directAlpha = (_dynamicFrame.transformationFromWorld().getAlphaRad());
					destination = new Frame(_dynamicFrame.transformationFromWorld())
							.setAlphaRad(_dynamicFrame.getAlphaRad() + Math.PI / 2)
							.setZ(hight)
							.setBetaRad(0).setGammaRad(Math.toRadians(-180));
					if (destination.getAlphaRad() < 0)
						destination.setAlphaRad(destination.getAlphaRad() + Math.PI);
					else
						destination.setAlphaRad(destination.getAlphaRad() - Math.PI);

					if (_dynamicFrame.getX() > _startingOffset) {
						if (once) {
							getLogger().info(String.format("A(deg) = %.2f; %s", Math.toDegrees(_dynamicFrame.getAlphaRad()), _dynamicFrame.toString()));
							once = false;
						}
						rt.updateWithRealtimeSystem();
						rt.setDestination(destination);
						if (destination.isCloseTo(_lbr.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame()), 4,
								Math.PI)) {
							getLogger().error("time : " + rt.getRemainingTime());
							if (destination.getZ() > 115) {
								hight = 115;
							} else {
								_gripper.gripAsync();
								_endTrack = true;
								getLogger().info(
										String.format("DESTINATION = %.2f; DYNAMIC = %.2f, CURRENT = %.2f, DIFF = %.2f",
												Math.toDegrees(destination.getAlphaRad()),
												Math.toDegrees(_dynamicFrame.transformationFromWorld().getAlphaRad()),
												Math.toDegrees(_lbr
														.getCurrentCartesianPosition(_gripper.getDefaultMotionFrame())
														.getAlphaRad()),
												Math.toDegrees(directAlpha)
														- Math.toDegrees(_dynamicFrame.getAlphaRad())));
								setState(ApplicationState.Display);
							}
						}
					}
				} while (!_endTrack || destination.getY() <= -255);

				rt.stopMotion();
				_endTrack = false;
				break;

			case Display:
				_gripper.move(BasicMotions.ptp(getFrame("/displayPos")).setJointVelocityRel(0.2));
				PieceEvaluator.enable();
				container = _lbr.move(BasicMotions.positionHold(cartMode, -1, TimeUnit.MINUTES)
					.breakWhen(new JointTorqueCondition(JointEnum.J4, -5, 5)));
				PieceEvaluator.disable();
				if (refusePiece) {
					setState(ApplicationState.drop);
				} else {
					setState(ApplicationState.refill);
				}
				break;

			case refill:
				_gripper.move(BasicMotions.ptp(getFrame("/dropPieces")).setJointVelocityRel(0.2));
				_gripper.releaseAsync();
				ThreadUtil.milliSleep(300);
				setState(ApplicationState.waiting);
				break;

			case drop:
				_gripper.move(BasicMotions.ptp(getFrame("/dropPieces")).setJointVelocityRel(0.2));
				_gripper.releaseAsync();
				ThreadUtil.milliSleep(300);
				setState(ApplicationState.waiting);
				break;

			default:
			case shutdown:
				return;
			}
		}
	}

	@Override
	public void dispose() {
		_endTrack = false;
		_gripper.releaseAsync();
		if (_session != null)
			_session.close();
		getLogger().info(">> disposed.");
		super.dispose();
	}

	private void setState(ApplicationState newState) {
		if (_state != newState) {
			getLogger().info(">> State Update: " + _state.name() + " -> " + newState.name());
			_state = newState;
		}
	}
}

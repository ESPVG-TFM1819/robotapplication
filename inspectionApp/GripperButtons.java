package org.inspectionApp;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import com.kuka.roboticsAPI.applicationModel.tasks.CycleBehavior;
import com.kuka.roboticsAPI.applicationModel.tasks.RoboticsAPICyclicBackgroundTask;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKey;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyBar;
import com.kuka.roboticsAPI.uiModel.userKeys.IUserKeyListener;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyAlignment;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyEvent;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLED;
import com.kuka.roboticsAPI.uiModel.userKeys.UserKeyLEDSize;

import org.espvgKuka.workstation.PneumaticGripper;


public class GripperButtons extends RoboticsAPICyclicBackgroundTask {

	@Inject private PneumaticGripper _gripper;

    private class keyListener implements IUserKeyListener {
        @Override
        public void onKeyEvent(IUserKey key, UserKeyEvent event) {
            if (event == UserKeyEvent.FirstKeyDown) {
                _gripper.gripAsync();
                key.setLED(UserKeyAlignment.Middle, UserKeyLED.Green, UserKeyLEDSize.Normal);
            }
            if (event == UserKeyEvent.SecondKeyDown) {
                _gripper.releaseAsync();
                key.setLED(UserKeyAlignment.Middle, UserKeyLED.Grey, UserKeyLEDSize.Small);
            }
        }
    }

    private IUserKeyBar gripperBar;

    @Override
    public void initialize() {
        gripperBar = getApplicationUI().createUserKeyBar("Gripper");
        gripperBar.addDoubleUserKey(0, new keyListener(), true);
        gripperBar.publish();
        initializeCyclic(0, 1, TimeUnit.MINUTES, CycleBehavior.BestEffort);
    }

    @Override
    public void runCyclic() {}
}